import { HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject, throwError} from 'rxjs';
import { catchError, retry, switchMap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class HomesService {
  private _data$:Subject<any> = new BehaviorSubject([]);

  get data(){
    return this._data$.asObservable();
  }

  constructor(private http: HttpClient) { }

  baseURL = 'http://110.74.194.124:3034/api';

  updateData(data:any){
    this._data$.next(data);
  }


  // post
  postUser(userInfo: any, file: any) {
    if (file){
      const formData: FormData = new FormData();
      formData.append('image', file);
      this.http.post<any>(this.baseURL + '/images', formData
      )
        .pipe( // post data
          switchMap(
            (str) => this.http.post<any>(this.baseURL + '/articles' ,
              {
                title: userInfo.title,
                description: userInfo.description,
                published: true,
                image: str.url
              }).pipe(
              catchError(this.handleError)
            )
          )
        ).subscribe(
          value => this.getALlPosts(),
          error => console.log(error)
        )
    }else{
      this.http.post<any>(this.baseURL + '/articles' ,
        {
          title: userInfo.title,
          description: userInfo.description,
          published: true,
          image: "null"
        }).pipe(
        catchError(this.handleError)
      ).subscribe(
        value => this.getALlPosts(),
        error => console.log(error)
      )
    }

  }

  // Delete Data
  deleteUser(uid:number){
    this.http.delete(this.baseURL+ "/articles/" + uid)
    .pipe( // post data
      catchError(this.handleError)
    ).subscribe(
      value => this.getALlPosts(),
      error => {
        console.log(error)
      }
    )
  }

  // get all posts
  getALlPosts() {
    this.http.get<any>(this.baseURL + '/articles')
      .pipe(
        retry(3),
        catchError(this.handleError)
      ).subscribe(
        val=>this.updateData(val)
    )
  }

  // get user info by using id
  getUserById(uid: string) {
    return this.http.get<any>(this.baseURL + '/articles/' + uid
      )
      .pipe(
        retry(3),
        catchError(this.handleError)
      )
  }

  // update user info by id
  editUserById(uid: number, userInfo: any, file: any) {
    if(file){
      const formData: FormData = new FormData();
      formData.append('image', file);
      console.log("user ", userInfo)
      this.http.post<any>(this.baseURL + '/images', formData
      )
        .pipe(
          switchMap(
            (str): any =>
              this.http.patch<any>(this.baseURL + '/articles/' + uid,
                {
                  title: userInfo.title,
                  description: userInfo.description,
                  image: str.url
                } ).pipe(
                catchError(this.handleError)
              )

          )
        ).subscribe(
        val => this.getALlPosts(),
        error => {
          console.log("update fail!");
        }
      )
    }else{
      this.http.patch<any>(this.baseURL + '/articles/' + uid,
        {
          title: userInfo.title,
          description: userInfo.description,
          image: userInfo.image
        } ).pipe(
        catchError(this.handleError)
      ).subscribe(
        val => this.getALlPosts(),
        error => {
          console.log("update fail!");
        }
      )

    }

  }

  // upload file to serve
  // uploadFile(image: any) {
  //   const formData: FormData = new FormData();
  //   formData.append('multipartFile', image);
  //   return this.http.post<any>(this.baseURL + '/article/images/', formData,
  //     {
  //       responseType: "text" as "json",
  //     }
  //   )
  //     .pipe(
  //       catchError(this.handleError)
  //     )
  // }


  // handle error ocurre during fetch or request data from server
  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
}
