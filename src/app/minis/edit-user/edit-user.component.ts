import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HomesService } from '../service/homes.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserInterface } from '../interface/users.interface';
import { Observable } from 'rxjs';
import { concatMap } from 'rxjs/operators';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  formEdit!: FormGroup
  id!: number
  sub: any
  userObj: any
  result!:Observable<any>
  file!: File;

  constructor(
    private service: HomesService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private dialogRef: MatDialogRef<EditUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }


  ngOnInit(): void {
    this.formEdit = this.formBuilder.group({
      title: new FormControl(this.data.title, [Validators.required,Validators.minLength(4)]),
      description: new FormControl(this.data.description, [Validators.required]),
      fileName: new FormControl(this.data.fileName)
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get getTitle() {
    return this.formEdit.get('title') as FormControl
  }

  get getDescription() {
    return this.formEdit.get('description') as FormControl
  }

  get getImage() {
    return this.formEdit.get('imageS') as FormControl
  }

  onSubmit() {
    this.service.editUserById(this.data._id,this.formEdit.value,this.file)
    this.dialogRef.close();
  }

  onFileChange(event:any) {
    this.file = event.target.files[0];
  }

}
