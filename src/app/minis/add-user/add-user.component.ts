import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HomesService } from '../service/homes.service';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {


  file!: File;
  message: string = "Hola Mundo!"
  registerForm!:FormGroup;
  result!:Observable<any>

  constructor(
    private formBuilder: FormBuilder,
    private service : HomesService
  ) {
  }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      title:new FormControl('',[Validators.required, Validators.minLength(4)]),
      description: new FormControl('',[Validators.required]),
      image:new FormControl('',[Validators.required])
    })
  }

  get getTitle(){
    return this.registerForm.get('title')
  }

  get getDescription(){
    return this.registerForm.get('description')
  }

  get getFileName(){
    return this.registerForm.get('image')
  }

  onSubmit(){
    this.service.postUser(this.registerForm.value,this.file)
  }

  onFileChange(event: any) {
    this.file = event.target.files[0];
  }


}
