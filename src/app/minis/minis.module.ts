import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import { AppRoutingModule } from '../app-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import { AddUserComponent } from './add-user/add-user.component';
import { NotfoundComponent } from './notfound/notfound.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { ViewUserComponent } from './view-user/view-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditUserComponent } from './edit-user/edit-user.component';
import {MatDialogModule} from '@angular/material/dialog';


const MaterialElement = [
  MatCardModule,
  MatButtonModule,
  FlexLayoutModule,
  MatIconModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatInputModule,
  MatDialogModule
]


@NgModule({
  declarations: [
    HomeComponent,
    AddUserComponent,
    NotfoundComponent,
    ViewUserComponent,
    EditUserComponent
  ],
  imports: [
    CommonModule,
    MaterialElement,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports:[
    HomeComponent,
    MaterialElement,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class MinisModule { }
