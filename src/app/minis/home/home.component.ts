import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { concat, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { HomesService } from '../service/homes.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private homeServices: HomesService,
    private router: Router,
    public dialog: MatDialog
  ) { }

  datas!:any[];

  openDialog(element: any): void {
    const dialogRef = this.dialog.open(EditUserComponent, {
      width: '500px',
      data: element
    });
    dialogRef.afterClosed().subscribe(result => console.log(result));
  }

  ngOnInit(): void {
    this.homeServices.getALlPosts();
  this.homeServices.data.subscribe(A=>this.datas = A.data)
  }

  goToAdd() {
    this.router.navigate(['home', 'add']).then(result => result)
  }

  goToView(id: string) {
    this.router.navigate(['home/view/', id])
  }

  goToUpdate(id: number, obj: any) {
    this.router.navigate(['home/edit/', id])
  }

  goToDelete(uid: number) {
    this.homeServices.deleteUser(uid)
  }


  // handleCreate(event:any){
  //   this.allPosts = this.homeServices.getALlPosts()
  // }

}
