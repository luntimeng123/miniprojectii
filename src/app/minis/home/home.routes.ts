import { AddUserComponent } from "../add-user/add-user.component";
import { EditUserComponent } from "../edit-user/edit-user.component";
import { NotfoundComponent } from "../notfound/notfound.component";

export const HOMES_ROUTES = [
    { 
        path: 'add',
        component: AddUserComponent
    },
    {
        path:'edit/:userId',
        component: EditUserComponent 
    }
  ];