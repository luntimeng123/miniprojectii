import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { HomesService } from '../service/homes.service';



@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss']
})
export class ViewUserComponent implements OnInit {

  id!:string
  sub:any
  userObj: any

  constructor(
    private route: ActivatedRoute,
    private homeService : HomesService
    ) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
       this.id = params['id']; // (+) converts string 'id' to a number

       // In a real app: dispatch action to load the details here.
    });
    this.homeService.getUserById(this.id).subscribe(
      val => this.userObj = val.data
    )
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }


}
