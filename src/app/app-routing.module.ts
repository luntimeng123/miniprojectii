import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './minis/home/home.component';
import { HOMES_ROUTES } from './minis/home/home.routes';
import { NotfoundComponent } from './minis/notfound/notfound.component';
import { ViewUserComponent } from './minis/view-user/view-user.component';

const routes: Routes = [
  {path:'home',component:HomeComponent,children:HOMES_ROUTES},
  {path:'home',component:HomeComponent},
  {path:'',redirectTo:'home',pathMatch:'full'},
  {path:'home/view/:id',component:ViewUserComponent},
  {path:'**',component:NotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule { }
